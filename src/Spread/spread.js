// const arr = [1, 2, 3, 4];
// const arr2 = [...arr];
// const arr3 = [...arr, 5, 6, 7];
// console.log(arr);
// console.log(arr2);
// console.log(arr3);

const obg = {
  name: "hesam",
  family: "feizollahi",
};

const obg2 = {
  ...obg,
  name: "pouya",
  age: 14,
};

console.log(obg);
console.log(obg2);
