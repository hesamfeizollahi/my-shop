import React from "react";
import { render } from "react-dom";
import { BrowserRouter, Route, Routes } from "react-router-dom";
// import "bootstrap/dist/css/bootstrap.min.css";
// import { Provider } from "react-redux";
// import configureStore from "./Redux/shop-project/store/confidureStore";
// import defaultState from "./Redux/shop-project/store/defaultState";
// import App from './shop-project/App'
// import registerServiceWorker from "./registerServiceWorker";
// import App from "./Redux/shop-project/App";

// final-project
import "./Final_Project/css/index.css";
import "bootstrap-v4-rtl/dist/css/bootstrap-rtl.css";
import "font-awesome/css/font-awesome.css";
import App from "./Final_Project/App";
import "./Final_Project/css/navbar.css";
import "./Final_Project/css/sidebar.css";
import "./Final_Project/css/login.css";
import "./Final_Project/css/page-404.css";
import "./Final_Project/css/animations.css";
import Login from "./Final_Project/Components/login";
import NotFound from "./Final_Project/Components/notFound";
import Courses from "./Final_Project/Components/courses";
import Education from "./Final_Project/Components/edu";
import Skill from "./Final_Project/Components/skill";
import About from "./Final_Project/Components/about";
import Contact from "./Final_Project/Components/contact";
import Post from "./Final_Project/Components/posts";
import Dashboard from "./Final_Project/Components/Admin/dashboard";
import CreatePost from "./Final_Project/Components/Admin/createPost";
import AllCourses from "./Final_Project/Components/Admin/allCourses";
import AllPosts from "./Final_Project/Components/Admin/allPosts";

const rootNode = document.getElementById("root");

// // jalase 5
// let formatName = (name,family) => {
//     return `${name} ${family}`
// }

// let user = {
//     name:'hesam',
//     family:'feizollahi'
// }

// let element = (
//     <div>
//         <h1>hello {formatName(user.name,user.family)}</h1>
//     </div>
// )

// ReactDOM.render(
//     element,
//     document.getElementById('root')
// )

// // jalase 6

// let greeting = (user) => {
//     if(user){
//         return <h1>hello {user.name} {user.family}</h1>
//     }
//     return <h1>user not found</h1>
// }

// let user = {
//     name:'hesam',
//     family:'feizollahi'
// }

// let element = (
//     <div>
//         {greeting(user)}
//     </div>
// )

// ReactDOM.render(
//     element,
//     document.getElementById('root')
// )

// // jalase 7

// let tick = () => {
//     const element = (
//         <div>
//             <h1>it is {new Date().toLocaleTimeString()}</h1>
//         </div>
//     )
//     ReactDOM.render(
//         element,
//         document.getElementById('root')
//     )
// }
// setInterval(tick,1000)

// const Welcome = (props) => {
//     return <h1>hello {props.name}</h1>
// }

// const element = (
//     <div>
//         <Welcome name='hesam'/>
//     </div>
// ) // این یک المنت ساده هست و برای صدا زدنش فقط کافیه اسمش رو بنویسیم element
// const App = () => {
//     return (
//         <div>
//             <Welcome name='hesam'/>
//             <Welcome name='ali'/>
//             <Welcome name='hossein'/>
//             <Welcome name='amir'/>
//         </div>
//     )
// } // این یک فانکشن هست که چیزی رو برمیگردونه پس باید به این شکل صدا بشه <App/>
// // const element = <Welcome name='hesam'/>
// ReactDOM.render(
//     <App/>,
//     document.getElementById('root')
// )

// // jalase 8

// let user = (a,b) => {
//     a = 10
//     return a + b
// } // یک مثال از کامپوننتی که خالص نیست

// // jalase 9

// // let Clock = (props) => {
// //     return (
// //         <h1>it is {props.date.toLocaleTimeString()}</h1>
// //     )
// // }

// // let tick = () => {
// //     ReactDOM.render(
// //         <Clock date= {new Date()}/>,
// //         document.getElementById('root')
// //     )
// // }

// // setInterval(tick,1000)

// class Clock extends React.Component {
//     constructor(props){ //2
//         super(props)
//         this.state = {date: new Date()}
//     }
//     componentDidMount(){ //4
//         this.timerId = setInterval(
//             () => this.tick(),
//             1000
//         )
//     }
//     componentWillUnmount(){
//         clearInterval(this.timerId)
//     }
//     tick(){ //5
//         this.setState({
//             date: new Date()
//         })
//     }

//     render(){ //3
//         return(
//             <h1>{this.state.date.toLocaleTimeString()}</h1>
//         )
//     }
// }

// ReactDOM.render( //1
//     <Clock/>,
//     document.getElementById('root')
// )

// // jalse 10
// class App extends React.Component{
//     constructor(props){
//         super(props)
//         this.state = {
//             isToggleOn: true
//         }
//         this.handler = this.handler.bind(this)
//     }
//     handler(){
//         this.setState(prevState => ({
//             isToggleOn: !prevState.isToggleOn
//         }))
//     }
//     render(){
//         return(
//             <button onClick={this.handler}>
//                 {this.state.isToggleOn ? 'on' : 'off'}
//             </button>
//         )
//     }
// }
// ReactDOM.render(
//     <App/>,
//     document.getElementById('root')
// )

// // jalse 11

// class Clock extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {date: new Date()}
//     }

//     componentDidMount(){
//         this.timerID = setInterval(
//             () => this.tick(),
//             1000
//         )
//     }

//     componentWillUnmount(){
//         clearInterval(this.timerID)
//     }

//     tick(){
//         this.setState({
//             date: new Date()
//         })
//     }

//     render(){
//         return(
//             <div>
//                 {this.state.date.toLocaleTimeString()}
//             </div>
//         )
//     }
// }

// ReactDOM.render(
//     <Clock/>,
//     document.getElementById('root')
// )

// // jalase 12

// class Toggle extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {isToggleOn: true}
//         this.handleClick = this.handleClick.bind(this)
//     }

//     handleClick(){
//         this.setState(prevState => ({
//             isToggleOn: !prevState.isToggleOn
//         }))
//     }

//     render(){
//         return(
//             <button onClick={this.handleClick}>
//                 {this.state.isToggleOn ? 'on' : 'off'}
//             </button>
//         )
//     }
// }

// ReactDOM.render(
//     <Toggle/>,
//     document.getElementById('root')
// )

// // jalase 13

// // let UserGreeting = () => {
// //     return <h1>logged in</h1>
// // }

// // let GuestGreeting = () => {
// //     return <h1>please Login</h1>
// // }

// // let Greeting = (props) => {
// //     let isLoggedIn = props.isLoggedIn
// //     if(isLoggedIn){
// //         return <UserGreeting/>
// //     }
// //     return <GuestGreeting/>
// // }

// // ReactDOM.render(
// //     <Greeting isLoggedIn={false}/>,
// //     document.getElementById('root')
// // )

// let LoginButton = (props) => {
//     return (
//         <button onClick={props.onClick}>
//             Login
//         </button>
//     )
// }

// let LogoutButton = (props) => {
//     return (
//         <button onClick={props.onClick}>
//             Logout
//         </button>
//     )
// }

// class LoginControl extends React.Component{
//     constructor(props){
//         super(props)
//         this.state = {isLoggedIn: true}
//         this.handleLoginClick = this.handleLoginClick.bind(this)
//         this.handleLogoutClick = this.handleLogoutClick.bind(this)
//     }
//     handleLoginClick(){
//         this.setState({
//             isLoggedIn: true
//         })
//     }

//     handleLogoutClick(){
//         this.setState({
//             isLoggedIn: false
//         })
//     }

//     render(){
//         const isLoggedIn = this.state.isLoggedIn
//         let button
//         if(isLoggedIn){
//             button = <LogoutButton onClick={this.handleLogoutClick}/>
//         } else {
//             button = <LoginButton onClick={this.handleLoginClick}/>
//         }
//         return (
//             <div>
//                 {button}
//                 <h1>
//                     {isLoggedIn ? 'welcome' : 'please Login'}
//                 </h1>
//             </div>
//         )
//     }
// }

// ReactDOM.render(
//     <LoginControl/>,
//     document.getElementById('root')
// )

// // اگر بخوایم یک کامپوننت اجرا نشه باید نال برگردونیم

// // jalase 14

// let randomColor = () => {
//     return '#' + Math.random().toString(16).substr(-6)
// }

// class Card extends Component {
//     render() {
//         const style = {
//             padding: 20,
//             textAlign: 'center',
//             color: 'white',
//             backgroundColor:this.props.color
//         }

//         return (
//             <div style={style}>
//                 {this.props.children}
//             </div>
//         )
//     }
// }

// class App extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {color: randomColor()}
//         this.randomizeColor = this.randomizeColor.bind(this)
//     }

//     randomizeColor() {
//         this.setState({
//             color: randomColor()
//         })
//     }

//     render() {
//         const {color} = this.state
//         const style = {
//             padding: 20
//         }
//         return (
//             <div style={style}>
//                 <Card color={color}>
//                     <input
//                         type={'button'}
//                         value={'تولید رنگ تصادفی'}
//                         onClick={this.randomizeColor}
//                     />
//                 </Card>
//             </div>
//         )
//     }
// }

// render(
//     <App/>,
//     document.getElementById('root')
// )

// // jalase 15

// // const numbers = [1,2,3,4,5]
// // const listItems = numbers.map(number => <li>{number * 2}</li>)
// // render(
// //     <ul>{listItems}</ul>,
// //     rootNode
// // )

// // let ListItems = props => {
// //     const numbers = props.numbers
// //     const listItems = numbers.map(number => <li key={number.toString()}>{number}</li>)

// //     return(
// //         <ul>
// //             {listItems}
// //         </ul>
// //     )
// // }

// // const numbers = [1,2,3,4,5]

// // render(
// //     <ListItems numbers={numbers}/>,
// //     rootNode
// // )

// let ListItems = props => {
//     return (
//         <li>
//             {props.value}
//         </li>
//     )
// }

// let NumbersList = props => {
//     const numbers = props.numbers
//     const numberlists = numbers.map(number => <ListItems value={number} key={number.toString()}/>)
//     return (
//         <ul>
//             {numberlists}
//         </ul>
//     )
// }

// const numbers = [1,2,3,4,5]

// render(
//     <NumbersList numbers={numbers}/>,
//     rootNode
// )

// jalase 16

// یک دیتابیس ساده ساخت و ازش استفاده کرد

// // jalse 17

// class NameForm extends Component {
//     constructor(props) {
//         super(props)
//         this.state = {value: ''}
//         this.handleChange = this.handleChange.bind(this)
//         this.handleSubmit = this.handleSubmit.bind(this)
//     }

//     handleSubmit(e) {
//         alert(`the name is: ${this.state.value}`)
//         e.preventDefault() // رفتار پیشفرض سابمیت رفرش صفحه هست ک با اینکار حذفش میکنیم
//         this.setState({value:''})
//     }

//     handleChange(e) {
//         this.setState({
//             value: e.target.value
//         })
//     }
//     render(){
//         const {value} = this.state
//         return(
//             <form onSubmit={this.handleSubmit}>
//                 <label>
//                     {this.state.value}
//                 </label>
//                 <br/>
//                 <label>
//                     name:
//                         <input type='text' value={value} onChange={this.handleChange}/>
//                 </label>
//                 <input type='submit' value='submit'/>
//             </form>
//         )
//     }
// }

// render(
//     <NameForm/>,
//     rootNode
// )

// // jalase 18

// reactstrap & bootstrap

// // jalase 19
// // وراثت
// <div>
//     {props.children}
// </div>

// // jalse 20

// render(<App />, rootNode);

// // jalase 21
// // کامپوننت ها با اولویت بالا

// const Info = ({ name }) => {
//   return (
//     <div>
//       <h1>اطلاعات شما</h1>
//       <p>نام کاربری : {name}</p>
//     </div>
//   );
// };

// const authInfo = (Component) => {
//   return (props) => (
//     <div>
//       {props.isLoggedin ? <Component {...props} /> : <p>لطفا لاگین کنید</p>}
//     </div>
//   );
// };

// const Authenticated = authInfo(Info);

// render(<Authenticated isLoggedin={false} name="hesam" />, rootNode);

// // jalase 22
// // shop-project-with-redux

// const store = configureStore(defaultState);

// render(
//   <Provider store={store}>
//     <App />
//   </Provider>,
//   rootNode
// );
// registerServiceWorker();

// jalase 23

// پروژه نهایی

render(
  <BrowserRouter>
    <Routes>
      <Route path="login" element={<Login />} />

      {/* اگر روتی پیدا نشد کافیه که قبل از پث کلمه کلیدی ایندکس رو قرار بدیم */}
      <Route path="admin" element={<Dashboard />}>
        <Route path="create-post" element={<CreatePost />} />
        <Route path="create-course" element={<CreatePost />} />
        <Route path="allcourses" element={<AllCourses />} />
        <Route path="allposts" element={<AllPosts />} />
      </Route>

      <Route path="" element={<App />}>
        <Route path="courses" element={<Courses />} />
        <Route path="education" element={<Education />} />
        <Route path="skill" element={<Skill />} />
        <Route path="about" element={<About />} />
        <Route path="contact" element={<Contact />} />
        <Route path="" element={<Post />} />
      </Route>
      <Route path="*" element={<NotFound />} />
      {/* پث ستاره به معنی صفحه ای هست که وجود نداره */}
    </Routes>
  </BrowserRouter>,
  rootNode
);
