import React from "react";
import { connect } from "react-redux";
import Counter from "./counter";

const Counters = ({ counters }) => {
  console.log(counters)
  return (
    <div>
      {counters.map(counter =>
        <Counter key={counter.id} counter={counter} />
      )}
    </div>
  );
};

export default connect((state) => {
  return {
    counters: state,
  };
})(Counters);
