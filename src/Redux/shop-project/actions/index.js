export const incrementCounter = (counter) => ({
  type: "increment",
  counter,
});

export const decrementCounter = (counter) => ({
  type: "decrement",
  counter,
});

export const deleteCounter = (counterId) => ({
  type: "delete",
  counterId,
});

export const resetCounters = () => ({
  type: "reset",
});
