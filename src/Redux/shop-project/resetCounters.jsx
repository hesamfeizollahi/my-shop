import React from "react";
import resetCss from "./counterCss.module.css";
import { connect } from "react-redux";
import { resetCounters } from "./actions/index";

const Reset = ({ dispatch }) => {
  return (
    <button
      className={resetCss.reset}
      onClick={() => dispatch(resetCounters())}
    >
      Reset
    </button>
  );
};

export default connect((state) => {
  return {
    state,
  };
})(Reset);
