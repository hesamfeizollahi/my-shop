import React from "react";
import navBarCss from "./counterCss.module.css";
import { connect } from "react-redux";

const NavBar = ({counter}) => {
  console.log(counter)
  return (
    <div className={navBarCss.navContainer}>
      <h1 className={navBarCss.shopName}>shop</h1>
      <h2 className={navBarCss.shopCount}>{counter}</h2>
    </div>
  );
};

export default connect((state) => {
  return {
    counter: state.filter((c) => c.value > 0).length,
  };
})(NavBar);
