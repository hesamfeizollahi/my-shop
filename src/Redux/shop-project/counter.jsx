import React from "react";
import counterCss from "./counterCss.module.css";
import { connect } from "react-redux";
import {
  incrementCounter,
  decrementCounter,
  deleteCounter,
} from "./actions/index";

const Counter = ({ dispatch, counter }) => {
  console.log(counter)
  return (
    <div className={counterCss.container}>
      <button
        className={counterCss.increase}
        onClick={() => dispatch(incrementCounter(counter))}
      >
        Increase
      </button>
      <button
        className={counterCss.decrease}
        onClick={() => dispatch(decrementCounter(counter))}
      >
        Decrease
      </button>
      <h1 className={counterCss.number}>{counter.value}</h1>
      <button
        className={counterCss.delete}
        onClick={() => dispatch(deleteCounter(counter.id))}
      >
        Delete
      </button>
    </div>
  );
};

export default connect((state) => {
  return {
    state,
  };
})(Counter);
