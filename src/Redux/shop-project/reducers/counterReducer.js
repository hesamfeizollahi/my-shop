export default (state = [], action) => {
  switch (action.type) {
    case "increment":
      const counters = [...state];
      const index = counters.indexOf(action.counter);
      counters[index] = { ...action.counter };
      counters[index].value++;
      return [...counters];
    case "decrement":
      const decCounters = [...state];
      const decIndex = decCounters.indexOf(action.counter);
      decCounters[decIndex] = { ...action.counter };
      decCounters[decIndex].value--;
      return [...decCounters];
    case "delete":
      const deleteCounters = state.filter((c) => c.id !== action.counterId);
      return [...deleteCounters];
    case "reset":
      const resetCounters = state.map((c) => {
        c.value = 0;
        return c;
      });
      return [...resetCounters];
    default:
      return state;
  }
};
