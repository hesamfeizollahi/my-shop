import React from "react";
import Counters from "./counters";
import NavBar from "./navBar";
import Reset from "./resetCounters";
import { connect } from "react-redux";

const App = () => {
  return (
    <React.Fragment>
      <NavBar />
      <Counters />
      <Reset />
    </React.Fragment>
  );
};

export default connect((state) => {
  return {
    state,
  };
})(App);
