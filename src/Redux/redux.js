// jalase 1

// const { createStore } = require("redux");
// const store = createStore((state = { count: 0 }, action) => { // اول این اجرا میشه
//   if (action.type === "hesam") {
//     // تایپ کلمه کلیدی هست و بجاش نمیشه کلمه دیگه ای بکار برد مگر اینکه تعریف شده باشه
//     return {
//       count: state.count + 1,
//     };
//   } else {
//     return state;
//   }
//   //   return state;
// });

// store.dispatch({ // دوم این اجرا میشه
//   type: "hesam",
// });
// console.log(store.getState()); // سوم این اجرا میشه

// const { createStore } = require("redux");
// const store = createStore((state = { name: "hesam" }, action) => {
//   if (action.type === "hesam") {
//     return {
//       name: state.name + " feizollahi",
//     };
//   } else {
//     return state;
//   }
// });

// store.dispatch({
//   type: "hesam",
// });
// console.log(store.getState());

// // jalse 2

// const { createStore } = require("redux");
// const store = createStore((state = { count: 0 }, action) => {
//   switch (action.type) {
//     case "increment":
//       return { count: state.count + 1 };
//     case "decrement":
//       return { count: state.count - 1 };
//     case "set":
//       return {
//         count: action.count, // وقتی پایین اکشن رو با ست صدا میزنیم بهش مقدار کونت روهم میدیم و اعمال میکنه
//       };
//     default:
//       return state;
//   }
// });

// store.subscribe(() => {
//   console.log(store.getState());
// }); // هر بار که استور عملیاتی انجام بده این یکبار عملیات درون خودش رو اجرا میکنه

// store.dispatch({
//   type: "increment",
// });

// store.dispatch({
//   type: "increment",
// });

// store.dispatch({
//   type: "decrement",
// });

// store.dispatch({
//   type: "set",
//   count: 100,
// });

// console.log(store.getState());

// jalase 3

// const { createStore } = require("redux");
// const store = createStore((state = { count: 0 }, action) => {
//   switch (action.type) {
//     case "increment":
//       const incrementBy =
//         typeof action.incrementBy === "number" ? action.incrementBy : 1;
//       return {
//         count: state.count + incrementBy,
//       };
//     case "decrement":
//       return {
//         count: state.count - 1,
//       };
//     case "set":
//       return {
//         count: action.setNumber,
//       };
//     case "reset":
//       return {
//         count: 0,
//       };
//     default:
//       return state;
//   }
// });

// store.subscribe(() => {
//   console.log(store.getState());
// });

// store.dispatch({
//   type: "increment",
//   incrementBy: 10,
// });

// store.dispatch({
//   type: "decrement",
// });

// store.dispatch({
//   type: "set",
//   setNumber: 5,
// });

// store.dispatch({
//   type: "reset",
// });

// // از روش اکشن جنریتور

// const store = createStore((state = { count: 0 }, action) => {
//   switch (action.type) {
//     case "increment":
//       return {
//         count: state.count + action.incrementBy,
//       };
//     case "decrement":
//       return {
//         count: state.count - action.decrementBy,
//       };
//     case "set":
//       return {
//         count: action.setNumber,
//       };
//     case "reset":
//       return {
//         count: 0,
//       };
//     default:
//       return state;
//   }
// });

// store.subscribe(() => {
//   console.log(store.getState());
// });
// // const incrementBy = () => ({})
// // وقتی اینطوری مینویسیم جاوا اسکریپت خودش میفهمه و ریترن میکنه و نیازی نیست ک ریترن رو درون فانکشن بنویسیم
// const incrementBy = (uns = { incrementBy: 1 }) => ({
//   type: "increment",
//   incrementBy: typeof uns.incrementBy === "number" ? uns.incrementBy : 1,
// });

// const decrementBy = (uns = { decrementBy: 1 }) => ({
//   type: "decrement",
//   decrementBy: typeof uns.decrementBy === "number" ? uns.decrementBy : 1,
// });

// store.dispatch(incrementBy());

// store.dispatch(incrementBy({ incrementBy: 2 }));

// store.dispatch(decrementBy({ decrementBy: 20 }));

// store.dispatch({
//   type: "set",
//   setNumber: 5,
// });

// store.dispatch({
//   type: "reset",
// });

// //  jalase 4

// const { createStore } = require("redux");
// // ریدوسر ها وظیفه تغییر استیت رو دارند
// // همون کدایی که بالا توی استور مینوشتیم هم ریدوسر بود الان فقط جداشون کردیم
// const reducer = (state = { count: 0 }, action) => {
//   switch (action.type) {
//     case "increment":
//       return {
//         count: state.count + action.incrementBy,
//       };
//     case "decrement":
//       return {
//         count: state.count - action.decrementBy,
//       };
//     case "set":
//       return {
//         count: action.setNumber,
//       };
//     case "reset":
//       return {
//         count: 0,
//       };
//     default:
//       return state;
//   }
// };

// const store = createStore(reducer);

// store.subscribe(() => {
//   console.log(store.getState());
// });
// const incrementBy = (uns = { incrementBy: 1 }) => ({
//   type: "increment",
//   incrementBy: typeof uns.incrementBy === "number" ? uns.incrementBy : 1,
// });

// const decrementBy = (uns = { decrementBy: 1 }) => ({
//   type: "decrement",
//   decrementBy: typeof uns.decrementBy === "number" ? uns.decrementBy : 1,
// });

// store.dispatch(incrementBy());

// store.dispatch(incrementBy({ incrementBy: 2 }));

// store.dispatch(decrementBy({ decrementBy: 20 }));

// store.dispatch({
//   type: "set",
//   setNumber: 5,
// });

// store.dispatch({
//   type: "reset",
// });

// // jalase 5

// // combined reducers

// const { createStore, combineReducers } = require("redux");

// const firstReducer = (state = { name: "hesam" }, action) => {
//   switch (action.type) {
//     case "hello":
//       return {
//         name: "hesam",
//       };
//     default:
//       return state;
//   }
// };

// const secondReducer = (state = { count: 0 }, action) => {
//   switch (action.type) {
//     case "increment":
//       return {
//         count: state.count + 1,
//       };
//     default:
//       return state;
//   }
// };

// const store = createStore(
//   combineReducers({
//     firstReducer,
//     secondReducer,
//   })
// );

// store.subscribe(() => {
//   console.log(store.getState());
// });

// store.dispatch({
//   type: "hello",
// });

// store.dispatch({
//   type: "increment",
// });

// jalase 6
