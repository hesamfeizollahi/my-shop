import React, { Component } from "react";
import { Container, Button } from "reactstrap";
import NavBar from "./components/navBar";
import Counters from "./components/counters";

class App extends Component {
  state = {
    counters: [
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 0 },
      { id: 4, value: 0 },
      { id: 5, value: 0 },
    ],
    // shopCounter: 0,
  };
  handleIncrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    counters[index].value++;
    this.setState({ counters });
  };

  handleDecrement = (counter) => {
    const counters = [...this.state.counters];
    const index = counters.indexOf(counter);
    counters[index] = { ...counter };
    if (counters[index].value > 0) {
      counters[index].value--;
    }
    this.setState({ counters });
  };

  handleDelete = (counter) => {
    const counters = this.state.counters.filter((c) => c.id !== counter.id);
    this.setState({ counters });
  };
  handleReset = () => {
    const counters = this.state.counters.map((c) => {
      c.value = 0;
      return c;
    });
    this.setState({ counters });
  };
  render() {
    const { counters } = this.state;
    return (
      <React.Fragment>
        <NavBar shopCounter={counters.filter((c) => c.value > 0).length} />
        <Container>
          <div style={{ float: "right" }}>
            <Counters
              counters={counters}
              onIncrement={this.handleIncrement}
              onDecrement={this.handleDecrement}
              onDelete={this.handleDelete}
            />
            <Button
              color="success"
              className="btn-sm m-2"
              onClick={this.handleReset}
            >
              ریستارت
            </Button>
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
