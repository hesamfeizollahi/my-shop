import React, { Component } from "react";
import Counter from "../components/counter";
import { Button, Container } from "reactstrap";

class Counters extends Component {
  render() {
    const { counters, onIncrement, onDecrement, onDelete } = this.props;
    return (
      <Container>
        <div style={{ float: "right" }}>
          {counters.map((item) => (
            <Counter
              counter={item}
              onIncrement={() => onIncrement(item)}
              onDecrement={() => onDecrement(item)}
              onDelete={() => onDelete(item)}
            />
          ))}
        </div>
      </Container>
    );
  }
}

export default Counters;
