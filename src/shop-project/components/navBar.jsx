import React, { Component } from "react";
import { Navbar, NavbarBrand, Badge } from "reactstrap";

class NavBar extends Component {
  render() {
    const { shopCounter } = this.props;
    return (
      <div style={{ direction: "rtl" }}>
        <Navbar color="dark" dark expand="md">
          <NavbarBrand href="/">
            سبد خرید
            <Badge color="info" pill className="m-2">
              {shopCounter}
            </Badge>
          </NavbarBrand>
        </Navbar>
      </div>
    );
  }
}

export default NavBar;
