import React, { Component } from "react";
import { Badge, Button } from "reactstrap";

class Counter extends Component {
  render() {
    const { onDecrement, counter, onIncrement, onDelete } = this.props;
    return (
      <React.Fragment>
        <Badge color={this.getBadgeColor()} className="m-2">
          {this.formatCount()}
        </Badge>
        <Button
          color="danger"
          className="btn-sm m-2"
          onClick={() => onDelete(counter)}
        >
          حذف
        </Button>
        <Button
          color="primary"
          className="btn-sm m-2"
          onClick={() => onIncrement(counter)}
        >
          افزایش
        </Button>
        <Button
          color="success"
          className="btn-sm m-2"
          onClick={() => onDecrement(counter)}
        >
          کاهش
        </Button>
        <br />
      </React.Fragment>
    );
  }
  getBadgeColor() {
    return this.props.counter.value === 0 ? "warning" : "primary";
  }
  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "صفر" : value;
  }
}

export default Counter;
