import React, { Component } from "react";
import { Outlet } from "react-router-dom";
import Footer from "./Components/footer";
import Navbar from "./Components/navBar";
import SideBar from "./Components/sideBar";

class App extends Component {
  render() {
    return (
      <div className="container-fluid rtl">
        <Navbar />
        <div className="row">
          <SideBar />
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <Outlet />
            {/* هرجا اینو بزاریم وقتی جای دیگه این کامپوننت رو به روت میدیم میتونیم بینش روت های
             دیگه رو قرار بدیم و به این ترتیب اون کامپوننت ها دقیقا در از مکان قرار میگیرن */}
          </main>
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
