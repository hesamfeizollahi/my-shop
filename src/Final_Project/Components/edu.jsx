import React from "react";

const Education = () => {
  return (
    <div className="container-fluid">
      <div className="card m-2">
        <p className="card-title text-center m-2">تحصیلات من</p>

        <div className="card-body">
          <ul className="list-group-item-danger">
            <li className="lead list-unstyled">
              <span className="fa fa-graduation-cap" /> تحصیلات
            </li>

            <li>دیپلم: رشته ریاضی مدرسه نمونه دولتی بندر انزلی</li>
            <li>لیسانس: رشته مهندسی شیمی دانشگاه علم و فناوری مازندران</li>
            <li>تخصص: برنامه نویسی وبسایت</li>
            <li>
              تجربه کار: سه سال آموزش و تجربه همکاری با یک استارت آپ به مدت یک
              سال
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Education;
