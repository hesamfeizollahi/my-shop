import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
const NotFound = () => {
  const navigate = useNavigate();
  useEffect(() => {
    navigate("/not-found");
  }, []);
  return (
    <React.Fragment>
      <div id="notfound">
        <div className="notfound">
          <div className="notfound-404">
            <h1>404</h1>
          </div>
          <h2>صفحه مورد نظر یافت نشد</h2>

          <Link to="/">
            <span className="arrow" />
            برگشت به صفحه اصلی
          </Link>
        </div>
      </div>
    </React.Fragment>
  );
};

export default NotFound;
