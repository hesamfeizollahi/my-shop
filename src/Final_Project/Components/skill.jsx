import React from "react";

const Skill = () => {
  return (
    <div className="container-fluid">
      <div className="card m-2">
        <p className="card-title text-center m-2">مهارت های من</p>
        <div className="card-body">
          HTML CSS
          <div className="progress m-2">
            <div
              className="progress-bar bg-primary progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "90%" }}
            >
              90%
            </div>
          </div>
          React JS
          <div className="progress m-2">
            <div
              className="progress-bar bg-danger progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "80%" }}
            >
              80%
            </div>
          </div>
          BootStrap
          <div className="progress m-2">
            <div
              className="progress-bar bg-warning progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "75%" }}
            >
              75%
            </div>
          </div>
          Wordpress
          <div className="progress m-2">
            <div
              className="progress-bar bg-success progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "95%" }}
            >
              95%
            </div>
          </div>
          Java Script
          <div className="progress m-2">
            <div
              className="progress-bar bg-dark progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "70%" }}
            >
              75%
            </div>
          </div>
          After Effect
          <div className="progress m-2">
            <div
              className="progress-bar bg-secondary progress-bar-striped progress-bar-css "
              role="progressbar"
              style={{ width: "50%" }}
            >
              50%
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Skill;
