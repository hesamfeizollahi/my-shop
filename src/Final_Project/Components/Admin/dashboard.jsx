import React, { Component } from "react";
import { Outlet } from "react-router-dom";
import Navbar from "./navBar";
import SideBar from "./sideBar";

class Dashboard extends Component {
  render() {
    return (
      <div className="container-fluid rtl">
        <Navbar />
        <div className="row">
          <SideBar />
          <main role="main" className="col-md-9 ml-sm-auto col-lg-10 px-4">
            <Outlet />
          </main>
        </div>
      </div>
    );
  }
}

export default Dashboard;
