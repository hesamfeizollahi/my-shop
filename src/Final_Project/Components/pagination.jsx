import React from "react";
import _ from "lodash";
import propTypes from "prop-types";

const Pagination = ({
  itemCount,
  pageSize,
  currentPage,
  onPageChange,
  handlePagePrev,
  handlePageNext,
}) => {
  const pageCount = Math.ceil(itemCount / pageSize);
  if (pageCount === 1) return null; // یعنی اگر تعداد پیج ها یکی باشد نال برگشت داده میشود در نتیجه پجینیت نشان داده نمیشود
  const pages = _.range(1, pageCount + 1);
  return (
    <nav>
      <ul className="pagination justify-content-center">
        <li className="page-item">
          <a
            className="page-link bg-dark"
            onClick={handlePagePrev}
            style={{ cursor: "pointer", color: "white" }}
          >
            prev
          </a>
        </li>
        {pages.map((page) => (
          <li className="page-item" key={page}>
            <a
              className={
                page === currentPage
                  ? "page-link bg-light"
                  : "page-link bg-dark"
              }
              style={
                page === currentPage
                  ? { cursor: "pointer", color: "black" }
                  : { cursor: "pointer", color: "white" }
              }
              onClick={() => onPageChange(page)}
            >
              {page}
            </a>
          </li>
        ))}
        <li className="page-item">
          <a
            className="page-link bg-dark"
            style={{ cursor: "pointer", color: "white" }}
            onClick={handlePageNext}
          >
            next
          </a>
        </li>
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  itemCount: propTypes.number.isRequired,
  pageSize: propTypes.number.isRequired,
  onPageChange: propTypes.func.isRequired,
  currentPage: propTypes.number.isRequired,
  handlePagePrev: propTypes.func.isRequired,
  handlePageNext: propTypes.func.isRequired,
};

export default Pagination;
