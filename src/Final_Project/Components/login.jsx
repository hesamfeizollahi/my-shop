import React from "react";
import { Link } from "react-router-dom";

const Login = () => {
  // history be onvane props be hameye component ha dade mishe va link ha ro kontrol mikone
  // const handleLogin = () => {
  //   location.pathname.replace("/saldl");
  // };
  return (
    <form className="rtl form-signin border rounded m-2 mx-auto bg-light shadow">
      <h1 className="h3 mb-3 font-weight-normal">لطفا وارد شوید</h1>
      <label htmlFor="inputEmail" className="sr-only">
        آدرس ایمیل
      </label>
      <input
        type="email"
        id="inputEmail"
        className="form-control"
        placeholder="آدرس ایمیل"
        // required
        // autoFocus
      />
      <label htmlFor="inputPassword" className="sr-only">
        کلمه عبور
      </label>
      <input
        type="password"
        id="inputPassword"
        className="form-control"
        placeholder="کلمه عبور"
        // required
      />

      <Link
        className="btn btn-lg btn-primary btn-block"
        type="submit"
        // onClick={handleLogin}
        to="/"
      >
        ورود
      </Link>
    </form>
  );
};

export default Login;
